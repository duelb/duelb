# duelb

do you even lift, bro?

## Getting Started

### Prerequisites

- We use `pnpm` package manager. Get it [here](https://pnpm.io/installation).
- `Tauri` environment must be prepared. Read more [here](https://v2.tauri.app/start/prerequisites/)

### Start the local dev

```bash
cd duelb
pnpm install
pnpm dev
```

## VSCode Setup

[VS Code](https://code.visualstudio.com/) + [Svelte](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode) + [Tauri](https://marketplace.visualstudio.com/items?itemName=tauri-apps.tauri-vscode) + [rust-analyzer](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer).
